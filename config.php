<?php

return [

    /* Content root absolute path. Will be used as a
     * base path for file listing. */
    "contentRoot" => __DIR__ . "/public/content",

    /* Mapping of the content root to public path. */
    "public" => "/content"

];
