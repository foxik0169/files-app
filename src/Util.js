
/**
 * Method that generates breadcrumbs from provided path.
 * @param {String} path
 * @returns {*}
 */
export const generateBreadcrumbs = (path) => {
  if (!path) return []

  let items = [{ title: 'root', href: '/' }]
  let segments = path.split('/').filter((i) => i !== '')

  let accumulator = ''
  if (segments) {
    segments.forEach((segment) => {
      accumulator += '/' + segment
      items.push({
        title: segment.trim(),
        href: accumulator
      })
    })
  }

  return items
}
