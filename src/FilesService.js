
class APIError extends Error {
  constructor (status, message) {
    super()

    this.message = message
    this.status = status
  }
}

class FilesService {
  constructor (baseUrl) {
    this.baseUrl = baseUrl
  }

  getDirectoryListing (path = '') {
    return fetch(this.baseUrl + path)
      .then(response => {
        if (response.status < 200 || response.status >= 300) throw new APIError(response.status, response.message)
        return response.json()
      })
  }
}

export default new FilesService('/api/index.php?path=')
