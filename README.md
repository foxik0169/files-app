# Files
![](https://img.shields.io/badge/version-1.0-brightgreen.svg)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![MIT Licence](https://badges.frapsoft.com/os/mit/mit.svg?v=103)](https://opensource.org/licenses/mit-license.php)

![Files](https://daniel.jmsystems.sk/static/files_safari.png)

Files is a simple web application which provides public accessible directory listing in a pleasant experience. 
Built with Vue.js and Symfony components for unmatched performance.

## Usage

To use Files, download and extract or clone this repository. The document root must be set to /public. As files
uses History API, you'll need to configure rewrite for index.html for every request except for files, directories 
and /api*. If you use Apache web server, a .htaccess file is provided out of the box, but you have to make sure
mod_rewrite is enabled. 

By default, the /content directory will contain the files that will be displayed in the application. 
You can change this in config.php in root of the project although the directory **must** be inside /public directory
for downloads to work. 

## Development

The source code for backend api is present in the /backend folder. You can inspect and edit it to suit your needs.

Frontend is implemented with Vue.js using vue-cli. To start development instance run `npm run dev`. If you want to 
build production version run `npm run build`. Sources are present in /src along with .scss styles.

## License

This application is licenced under MIT.

[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)
