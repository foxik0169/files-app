<?php

require("../../vendor/autoload.php");

use Files\API;
use Symfony\Component\HttpFoundation\Request;

$request = Request::createFromGlobals();

try {
    $api = new API($request);
    $api->run()
        ->prepare($request)
        ->send();
} catch (Exception $e) {
    (new \Files\APIResponse([
        "message" => $e->getMessage(),
        "file" => $e->getFile(),
        "line" => $e->getLine()
    ], 500))->prepare($request)->send();
}
