<?php namespace Files;

use JsonSerializable;

class ItemModel implements JsonSerializable
{

    private $name;
    private $type;
    private $size;
    private $publicPath;

    public function __construct($name, $type, $size, $publicPath)
    {
        $this->name = $name;
        $this->type = $type;
        $this->size = $size;
        $this->publicPath = $publicPath;
    }

    public function jsonSerialize()
    {
        return [
            'name' => $this->name,
            'type' => $this->type,
            'size' => $this->size,
            'publicPath' => $this->publicPath
        ];
    }

}
