<?php namespace Files;

use InvalidArgumentException;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class API
{
    const VERSION = "1.0";

    private $request;
    private $config;

    /**
     * @param Request $request
     */
    public function __construct($request)
    {
        $this->request = $request;
        $this->config = require(__DIR__ . "/../config.php");
    }

    /**
     * @return Response
     */
    public function run() {
        $path = $this->request->get('path');
        $contentRoot = $this->config['contentRoot'];

        $finder = new Finder();

        try {
            $finder->in($contentRoot . $path);
        } catch (InvalidArgumentException $e) {
            return new APIResponse(["message" => "Specified path doesn't exist."], 404);
        }

        $finder
            ->ignoreVCS(true)
            ->ignoreUnreadableDirs(true)
            ->ignoreDotFiles(true)
            ->sortByName()
            ->sortByType()
            ->depth(0);

        $items = [];

        foreach ($finder as $item) {
            $items[] = new ItemModel(
                $item->getBasename(),
                $item->getType(),
                $item->isFile() ? $item->getSize() : $this->getSubItemsCount($item->getPathname()),
                $this->getPublicPath($item));
        }

        return new APIResponse([
            'path' => $path,
            'items' => $items
        ]);
    }

    private function getSubItemsCount($path) {
        return (new Finder())->in($path)
            ->depth("== 0")
            ->count();
    }

    /**
     * @param SplFileInfo $file
     * @return string
     */
    private function getPublicPath($file) {
        return str_replace($this->config['contentRoot'], '', $file->getPathname());
    }

}
