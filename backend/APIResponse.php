<?php namespace Files;

use Symfony\Component\HttpFoundation\Response;

class APIResponse extends Response
{

    private $config;

    /**
     * @param array $content
     * @param int $statusCode
     */
    public function __construct($content = [], $statusCode = 200)
    {
        parent::__construct();

        $this->config = require(__DIR__ . "/../config.php");

        $this->setStatusCode($statusCode);
        $this->setContent(json_encode($this->getFormattedContent($content, $statusCode)));

        $this->headers->set("Content-type", "application/json");
        $this->headers->set("X-Files-API", API::VERSION);
    }

    private function getFormattedContent($content, $statusCode) {
        return [
            'version' => API::VERSION,
            'status' => $statusCode,
            'publicRoot' => $this->config['public'],
            'content' => $content,
        ];
    }

}
